from app import app
from detectors.vocabulary import Vocabulary
import os


app.config["IMAGE_UPLOADS"] = os.path.join(os.getcwd(), "app/static/images")

# make sure model files can be found here
app.config["ENCODER_PATH"] = os.path.join(os.getcwd(), "models/encoder-5-3000.pkl")
app.config["DECODER_PATH"] = os.path.join(os.getcwd(), "models/decoder-5-3000.pkl")
app.config["VOCAB_PATH"] = os.path.join(os.getcwd(), "models/vocab.pkl")

if not os.path.exists(app.config["IMAGE_UPLOADS"]):
    os.makedirs(app.config["IMAGE_UPLOADS"])


if __name__ == '__main__':
    app.run(debug=True)
