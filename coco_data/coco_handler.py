import os
import json
import numpy as np


class CocoHandler:
    def __init__(self,
                 annotation_file, images_dir):
        self._annotation_file = annotation_file
        self._images_dir = images_dir
        self._dataset = None
        self._annotations = None
        self._images = None
        self._image_name_to_id = None
        self._image_ids = None

    @property
    def dataset(self):
        if self._dataset is None:
            print('loading annotations into memory...')
            self._dataset = json.load(open(self._annotation_file, 'r'))
            assert type(self._dataset) == dict, 'annotation file format {} not supported'.format(type(self._dataset))
        return self._dataset

    @property
    def images(self):
        if self._images is None:
            self._images = {}
            if 'images' in self.dataset:
                for image in self.dataset["images"]:
                    self._images[image['id']] = image
        return self._images

    @property
    def annotations(self):
        if self._annotations is None:
            self._annotations = {}
            if 'annotations' in self.dataset:
                for ann in self.dataset["annotations"]:
                    self._annotations[ann["image_id"]] = ann["caption"]
        return self._annotations

    @property
    def image_name_to_id(self):
        if self._image_name_to_id is None:
            self._image_name_to_id = {}
            for image_id, image_info in self.images.items():
                self._image_name_to_id[image_info["file_name"]] = image_id
        return self._image_name_to_id

    @property
    def image_ids(self):
        if self._image_ids is None:
            self._image_ids = [img_id for img_id in self.images.keys()]
        return self._image_ids

    def get_caption_from_image_name(self, image):
        img_id = self.image_name_to_id[image]
        print(self.annotations[img_id])

    def get_random_image_id(self):
        idx = np.random.randint(len(self.image_ids))
        return self.image_ids[idx]

    def get_random_image_and_caption(self):
        img_id = self.get_random_image_id()
        # check if image exists in img dir
        img_path = os.path.join(self._images_dir, self.images[img_id]["file_name"])
        if self._images_dir is None or not os.path.isfile(img_path):
            img_path = self.images[img_id]["coco_url"]
        return img_path, self.annotations[img_id]


if __name__ == "__main__":

    cd = CocoHandler(annotation_file="/Users/lffstokes/Jobs/Logically/DataSets/coco/annotations/annotations/captions_train2014.json",
                     images_dir="/Users/lffstokes/Jobs/Logically/DataSets/coco/train2014")
    cd.get_caption_from_image_name("COCO_train2014_000000444010.jpg")