import os
from app import app
from detectors.coherence import CoherenceScore
from flask import url_for, request, render_template,  redirect, session


@app.route("/", methods=['GET', 'POST'])
def index():
    if request.method == "POST":
        print(request.form)
        if request.files:
            image = request.files["image"]
            print(image)
            session["image_file"] = os.path.join(app.config["IMAGE_UPLOADS"], image.filename)
            session["image_text"] = request.form["image_text"]

            print(session["image_file"])
            image.save(session["image_file"])

            print(session)
            return redirect(url_for('results'))

    return render_template("home.html")


@app.route("/results", methods=['GET', 'POST'])
def results():

    coh_score = CoherenceScore(encoder_path=app.config["ENCODER_PATH"],
                               decoder_path=app.config["DECODER_PATH"],
                               vocab_path=app.config["VOCAB_PATH"],
                               img_save_dir=app.config["IMAGE_UPLOADS"])
    coherence, generated_text = coh_score.coherence_score(session["image_file"], session["image_text"])
    print("Coherence score ", coherence)

    #name, suffix = os.path.basename(session["image_file"]).split(".")
    #img_annotated = "{}_annotated.{}".format(name, suffix)

    return render_template("results.html",
                           image_file=os.path.basename(session["image_file"]),
                           image_text=session["image_text"],
                           generated_text=generated_text,
                           coherence=coherence)
