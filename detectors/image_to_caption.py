import torch
from torchvision import transforms
from detectors.encoder_decoder import EncoderCNN, DecoderRNN
from PIL import Image


class ImageToCaption:
    def __init__(self, encoder_path, decoder_path, vocab, vocab_path=None,
                 embed_size=256, hidden_size=512, num_layers=1, device="cpu"):
        self._encoder_path = encoder_path
        self._decoder_path = decoder_path
        self._vocab_path = vocab_path
        self._embed_size = embed_size
        self._hidden_size = hidden_size
        self._num_layers = num_layers
        self._device = device
        self._encoder = None
        self._decoder = None
        self._vocab = vocab
        self._vocab_size = None
        self._transform = transforms.Compose([ transforms.ToTensor(),
                                               transforms.Normalize((0.485, 0.456, 0.406),
                                                                    (0.229, 0.224, 0.225))])

    @property
    def vocab_size(self):
        if self._vocab_size is None:
            self._vocab_size = self._vocab.idx
        return self._vocab_size

    @property
    def encoder(self):
        if self._encoder is None:
            self._encoder = EncoderCNN(self._embed_size).eval()
            self._encoder.to(self._device)
            self._encoder.load_state_dict(torch.load(self._encoder_path))
        return self._encoder

    @property
    def decoder(self):
        if self._decoder is None:
            self._decoder = DecoderRNN(self._embed_size, self._hidden_size, self.vocab_size, self._num_layers).eval()
            self._decoder.to(self._device)
            self._decoder.load_state_dict(torch.load(self._decoder_path))
        return self._decoder

    @staticmethod
    def load_image(image_path, transform=None):
        image = Image.open(image_path).convert('RGB')
        image = image.resize([224, 224], Image.LANCZOS)
        if transform is not None:
            image = transform(image).unsqueeze(0)
        return image

    def get_image_caption(self, image):
        image = self.load_image(image, self._transform)
        image_tensor = image.to(self._device)

        # Generate an caption from the image
        feature = self.encoder(image_tensor)
        sampled_ids = self.decoder.sample(feature)
        sampled_ids = sampled_ids[0].cpu().numpy()  # (1, max_seq_length) -> (max_seq_length)
        # Convert word_ids to words
        sampled_caption = []
        for word_id in sampled_ids:
            word = self._vocab.idx2word[word_id]
            if word == '<end>':
                break
            sampled_caption.append(word)
        if sampled_caption[0] == "<start>":
            sampled_caption.pop(0)
        sentence = ' '.join(sampled_caption)
        print(sentence)
        return sampled_caption


if __name__ == "__main__":

    i2c = ImageToCaption(encoder_path="../models/encoder-5-3000.pkl",
                         decoder_path="../models/decoder-5-3000.pkl",
                         vocab_path="../models/vocab.pkl")
    sample = i2c.get_image_caption("/Users/lffstokes/Jobs/Logically/DataSets/coco/val2014/COCO_val2014_000000290981.jpg")