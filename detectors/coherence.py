import os
import numpy as np
import pickle
from nltk.tokenize import word_tokenize
from coco_data.coco_handler import CocoHandler
from urllib.request import urlretrieve
from collections import Counter
from nltk.corpus import stopwords
from detectors.image_to_caption import ImageToCaption


class CoherenceScore:
    def __init__(self,
                 encoder_path="../models/encoder-5-3000.pkl",
                 decoder_path="../models/decoder-5-3000.pkl",
                 vocab_path="/Users/lffstokes/PycharmProjects/vlcompare/models/vocab.pkl",
                 img_save_dir="../app/static/images",
                 coco_annotations="../annotations/captions_val2014.json",
                 coco_img_dir="/Users/lffstokes/Jobs/Logically/DataSets/coco/train2014"):
        self._encoder_path = encoder_path
        self._decoder_path = decoder_path
        self._vocab_path = vocab_path
        self._coco_annotations = coco_annotations
        self._img_save_dir = img_save_dir
        self._coco_img_dir = coco_img_dir
        self._obj_det = None
        self._vocab = None
        self._coco_handler = None
        self._image_translator = None
        self._stop_words = stopwords.words('english')

    @property
    def image_translator(self):
        if self._image_translator is None:
            self._image_translator = ImageToCaption(encoder_path=self._encoder_path,
                                                    decoder_path=self._decoder_path,
                                                    vocab=self.vocab)
            return self._image_translator

    @property
    def vocab(self):
        if self._vocab is None:
            with open(self._vocab_path, 'rb') as f:
                self._vocab = pickle.load(f)
        return self._vocab

    @property
    def coco_handler(self):
        if self._coco_handler is None:
            self._coco_handler = CocoHandler(annotation_file=self._coco_annotations,
                                             images_dir=self._coco_img_dir)
        return self._coco_handler

    def get_random_coco_image_and_caption(self):
        img_path, caption = self.coco_handler.get_random_image_and_caption()
        # check id img_path is url
        if img_path.startswith("http:"):
            new_img_path = os.path.join(self._img_save_dir, os.path.basename(img_path))
            urlretrieve(img_path, new_img_path)
            img_path = new_img_path
        return img_path, caption

    @staticmethod
    def recall(reference, candidate):
        '''
        :param reference: list[str] words from coco caption or descriptive text
        :param candidate: list[str] objects from detector
        :return: float
        '''
        num_overlaps = 0
        ref_counter = Counter(reference)
        can_counter = Counter(candidate)

        # add plural in case there is more than one
        new_counter = Counter()
        for can in can_counter:
            if can_counter[can] > 1:
                new_can = can + "s"
                new_counter[new_can] = 1
        can_counter.update(new_counter)
        # look for overlapping objects
        for ref in ref_counter:
            if ref in can_counter:
                num_overlaps += min(ref_counter[ref], can_counter[ref])
        print("overlaps ", num_overlaps)
        if len(reference) == 0:
            return 0
        else:
            return num_overlaps / len(reference)

    def tokenize_string(self, text, remove_stopwords=False):
        if isinstance(text, str):
            words = word_tokenize(text)
        else:
            words = text
        # remove stop words and punctuation
        words = [word for word in words if word.isalpha()]
        if remove_stopwords:
            words = [word for word in words if word not in self._stop_words]
        return words

    def vectorise_word(self, word):
        if word not in self.vocab.word2idx:
            idx = self.vocab.word2idx['<unk>']
        else:
            idx = self.vocab.word2idx[word]
        vector = np.zeros(self.vocab.idx)
        vector[idx] = 1
        return vector

    def sum_of_vectors(self, words):
        vector = np.zeros(self.vocab.idx)
        for word in words:
            vector += self.vectorise_word(word)
        #vector = vector.reshape((-1, 1))
        return vector

    def random_coherence_score(self):
        '''
        :param input_text: string
        :return:
        '''
        img_path, reference_caption = self.get_random_coco_image_and_caption()
        return self.coherence_score(img_path, reference_caption)

    @staticmethod
    def cosine_similarity(x, y):
        z = np.dot(x, y)
        return np.linalg.norm(z)/(np.linalg.norm(x)*np.linalg.norm(y))

    def coherence_score(self, img_path, reference_text):
        generated_words = self.image_translator.get_image_caption(img_path)
        # order objects by size for relevance

        print("generated text: ", ' '.join(generated_words))
        print("submitted text: ", reference_text)
        image_word_tokens = self.tokenize_string(generated_words)
        reference_tokens = self.tokenize_string(reference_text)

        image_words_vector = self.sum_of_vectors(image_word_tokens)
        reference_vector = self.sum_of_vectors(reference_tokens)
        print(self.cosine_similarity(np.array([0,1]), np.array([0,1])))
        cos_similarity = round(self.cosine_similarity(image_words_vector, reference_vector), 2)

        recall = self.recall(self.tokenize_string(reference_text), self.tokenize_string(generated_words))
        print("recall ", recall)

        generated_words = " ".join(generated_words)
        return cos_similarity, generated_words


if __name__ == "__main__":

    cs = CoherenceScore()
    cs.random_coherence_score()
