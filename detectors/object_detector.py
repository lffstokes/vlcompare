import os
import cv2
#import detectron2
from detectron2.utils.logger import setup_logger
setup_logger()

# import some common libraries
import os
import cv2
import numpy as np
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog


class ObjectDetector:
    def __init__(self, model_file, config_file, confidence_threshold, img_save_dir):
        self._confidence_threshold = confidence_threshold
        self._model_file = model_file
        self._config_file = config_file
        self._img_save_dir = img_save_dir
        self._det = None

    @staticmethod
    def create_dir_if_not_exist(dir_path):
        if not os.path.isdir(dir_path):
            os.mkdir(dir_path)

    def save_img(self, dir_path, orig_img_name, img):
        self.create_dir_if_not_exist(dir_path)
        name, suffix = orig_img_name.split(".")
        new_img_path = os.path.join(dir_path, "{}_annotated.{}".format(name, suffix))
        cv2.imwrite(new_img_path, img)

    def det_objects(self, image, save_annotated_image):
        pass


class Detectron2Detector(ObjectDetector):
    def __init__(self, model_file, config_file, confidence_threshold, img_save_dir, model_device="cpu"):
        super(ObjectDetector, self).__init__(model_file=model_file,
                                             config_file=config_file,
                                             confidence_threshold=confidence_threshold,
                                             img_save_dir=img_save_dir)
        self._model_device = model_device
        self._metadata = None
        self._classes = None
        self._cfg = None

    @property
    def cfg(self):
        if self._cfg is None:
            self._cfg = get_cfg()
        return self._cfg

    @property
    def det(self):
        if self._det is None:
            self.cfg.merge_from_file(model_zoo.get_config_file(self._config_file))
            self.cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = self._confidence_threshold
            self.cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(self._model_file)
            self.cfg.MODEL.DEVICE = self._model_device
            self._det = DefaultPredictor(self.cfg)
        return self._det

    @property
    def metadata(self):
        if self._metadata is None:
            self._metadata = MetadataCatalog.get(self.cfg.DATASETS.TRAIN[0])
        return self._metadata

    @property
    def classes(self):
        if self._classes is None:
            self._classes = self.metadata.get("thing_classes", None)
        return self._classes

    @staticmethod
    def get_predicted_labels(classes, class_names):
        labels = []
        if classes is not None and class_names is not None and len(class_names) > 0:
            labels = [class_names[i] for i in classes]
        return labels

    def box_area(self, box):
        width = box[2] - box[0]
        height = box[3] - box[1]
        return width * height

    def det_objects(self, image, save_annotated_image):
        img = cv2.imread(image)
        predictions = self.det(img)
        objects = self.get_predicted_labels(predictions["instances"].pred_classes, self.classes)
        scores = predictions["instances"].scores.numpy()
        boxes = predictions["instances"].pred_boxes
        areas = [self.box_area(box).numpy() for box in boxes]
        # convert to numpy array
        areas = np.asarray(areas)
        objects = np.asarray(objects)

        if save_annotated_image:
            v = Visualizer(img[:, :, ::-1], MetadataCatalog.get(self.cfg.DATASETS.TRAIN[0]), scale=1.2)
            out = v.draw_instance_predictions(predictions["instances"].to("cpu"))
            labelled_img = out.get_image()[:, :, ::-1]
            self.save_img(self._img_save_dir, os.path.basename(image), labelled_img)
        return objects, scores, areas


if __name__ == "__main__":

    d2d = Detectron2Detector(confidence_threshold=0.5,
                             config_file="COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml",
                             model_file="COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml",
                             img_save_dir="/Users/lffstokes/Jobs/Logically/coherence/app/static/images")

    #d2d.det_objects(image="/Users/lffstokes/Jobs/Logically/DataSets/cocostuff/val2017/000000000139.jpg",
    #                save_annotated_image=True)
    #d2d.det_objects(image="/Users/lffstokes/Jobs/Logically/DataSets/cocostuff/val2017/000000000139.jpg",
    #                save_annotated_image=True)
    d2d.det_objects(image="/Users/lffstokes/Jobs/Logically/DataSets/coco/train2014/COCO_train2014_000000348907.jpg",
                    save_annotated_image=True)