import os
import numpy as np
from nltk.tokenize import word_tokenize
from detectors.object_detector import Detectron2Detector
from coco_data.coco_handler import CocoHandler
from urllib.request import urlretrieve
from collections import Counter
from nltk.corpus import stopwords


class CoherenceScoreOld:
    def __init__(self,
                 confidence_threshold=0.5,
                 config_file="COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml",
                 model_file="COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml",
                 img_save_dir="../app/static/images",
                 coco_annotations="../annotations/captions_train2014.json",
                 coco_img_dir="/Users/lffstokes/Jobs/Logically/DataSets/coco/train2014",
                 max_objects=5):
        self._confidence_threshold = confidence_threshold
        self._model_file = model_file
        self._config_file = config_file
        self._coco_annotations = coco_annotations
        self._img_save_dir = img_save_dir
        self._coco_img_dir = coco_img_dir
        self._max_objects = max_objects
        self._obj_det = None
        self._coco_handler = None
        self._stop_words = stopwords.words('english')

    @property
    def obj_det(self):
        if self._obj_det is None:
            self._obj_det = Detectron2Detector(confidence_threshold=self._confidence_threshold,
                                               model_file=self._model_file,
                                               config_file=self._config_file,
                                               img_save_dir=self._img_save_dir)
        return self._obj_det

    @property
    def coco_handler(self):
        if self._coco_handler is None:
            self._coco_handler = CocoHandler(annotation_file=self._coco_annotations,
                                             images_dir=self._img_save_dir)
        return self._coco_handler

    def get_random_coco_image_and_caption(self):
        img_path, caption = self.coco_handler.get_random_image_and_caption()
        # check id img_path is url
        if img_path.startswith("http:"):
            new_img_path = os.path.join(self._img_save_dir, os.path.basename(img_path))
            urlretrieve(img_path, new_img_path)
            img_path = new_img_path
        return img_path, caption

    @staticmethod
    def recall(reference, candidate):
        '''
        :param reference: list[str] words from coco caption or descriptive text
        :param candidate: list[str] objects from detector
        :return: float
        '''
        num_overlaps = 0
        ref_counter = Counter(reference)
        can_counter = Counter(candidate)

        # add plural in case there is more than one
        new_counter = Counter()
        for can in can_counter:
            if can_counter[can] > 1:
                new_can = can + "s"
                new_counter[new_can] = 1
        can_counter.update(new_counter)
        # look for overlapping objects
        for ref in ref_counter:
            if ref in can_counter:
                num_overlaps += min(ref_counter[ref], can_counter[ref])
        print("overlaps ", num_overlaps)
        return num_overlaps / len(reference)

    def tokenize_string(self, text):
        text = text.lower()
        words = word_tokenize(text)
        # remove stop words and punctuation
        words = [word for word in words if word.isalpha() and word not in self._stop_words]
        return words

    def random_coherence_score(self):
        '''
        :param input_text: string
        :return:
        '''
        img_path, caption = self.get_random_coco_image_and_caption()
        return self.coherence_score(img_path, caption)

    def coherence_score(self, img_path, description, save_annotated_image=True):
        objects, scores, areas = self.obj_det.det_objects(img_path, save_annotated_image=save_annotated_image)
        # order objects by size for relevance
        idx = np.argsort(areas)[::-1]
        objects = objects[idx]
        scores = scores[idx]
        areas = areas[idx]
        # take maximum number of objects
        relevant_objects = objects[:self._max_objects]

        print("detected objects ", relevant_objects)
        print("ground truth: ", description)
        recall = self.recall(self.tokenize_string(description), objects)
        print(recall)
        return recall


if __name__ == "__main__":

    cs = CoherenceScore()
    cs.random_coherence_score()

